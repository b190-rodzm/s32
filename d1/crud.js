// importing the "http" module
const http = require("http");

// storing the 400 in a variable called port
const port = 4000;

// Mock Database
let directory = [
	{
		"name": "Brandon",
		"email": "brandon@mail.com"
	},
	{
		"name": "Jobert",
		"email": "jobert@mail.com"
	}
];

// storing the createServer method inside the server variable
const server = http.createServer((request, response) => {


	// route for returning all items upon receiving a GET method request
	if (request.url === "/users" && request.method === "GET" ) {
		response.writeHead(200, { "Content-Type": "application/json" });
		response.write(JSON.stringify(directory));
		response.end();
	}

});

server.listen(port);
console.log(`Server now running at port: ${port}`);