const http = require("http");

const port = 4000;

// const server = http.createServer((request, response) => {
//   if (request.url === "/items") {
//   response.writeHead(200, {"Content-Type": "text/plain"} );
//   response.end("Data retrieved from the database");
//   }
// });

const server = http.createServer((request, response) => {

  if (request.url === "/items" && request.method === "GET") {
  response.writeHead(200, {"Content-Type": "text/plain"} );
  response.end("Data retrieved from the database");
  };

  if (request.url === "/items" && request.method === "POST") {
    response.writeHead(200, {"Content-Type": "text/plain"} );
    response.end("Data to be sent to the database");
    }

});

server.listen(port);
console.log(`Server now running at port: ${port}`);